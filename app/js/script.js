const quiz = {} //crée un objet
quiz.questionCourante = 0 //départ à la première question
quiz.reponses = [] //tableau pour entrer les reponses entrée par l'utilisateur


//questions du quizz
const donneesEntrantes =
    `
[
    {
        "question":"Quelle est la couleur du cheval blanc de Bonaparte",
        "reponses":["Blanc à pois vert","Noir","Rose"],
        "bonneReponse":"Blanc à pois vert"
        
        
    },
    {
        "question":"Quelle est la couleur d'une banane?",
        "reponses":["blue","jaune","rouge"],
        "reponse":1
        
    },
    {
        "question":"Combien d'oreille à un chien malentendant",
        "reponses":["une et demie","trois","deux"],
        "reponse":3
        
    },
    {
        "question":"La tomate est-elle un fruit ou un légume",
        "reponses":["un fruit","un légume","J'aime pas les tomates"],
        "reponse":1
        
    }
]
`
    //fin des questions

const questions = JSON.parse(donneesEntrantes)
const accordeon = $('#accordeon')

$(function() {
    AjouterValidations()

})


function AjouterValidations() {
    //$('aside').remove()

    $("form[name='validationForm']").validate({

        rules: {
            prenom: "required",
            nom: "required",
            email: {
                required: true,
                email: true
            },

            choix: "required",

        },
        messages: {
            prenom: "Veuillez entrer un prénom",
            nom: "Veuillez entrer un nom",
            email: "Veuillez entrez un courriel",
            choix: "Veuillez faire un choix"
        },

        submitHandler: function(form) {
            quiz.prenom = $('#prenom').val()
            quiz.nom = $('#nom').val()
            quiz.email = $('#email').val()
            quiz.statut = $('#statut').val()
            CreerQuiz()
            $("#validationForm").remove()
        },
    });
}


$('html').addClass('background-color').css('background-color', '#F1A94A')
$('body').addClass('background-color').css('background-color', '#F1A94A')
$('.soumettre_envoyer').css('background-color', '#D95728')
    .css('color', '#071029')
    .css('font-size', '1.5rem')


function CreerQuiz() { //affiche une question
    AfficherQuestionQuiz()
}

function AfficherQuestionQuiz() {

    const questionCourante = questions[quiz.questionCourante] //on a réduit dans un constante parce que ça se répetait beaucoup
    const question = $('<p class="mt-5 ml-4 p-4 row">' + questionCourante.question + '</p>').css('background-color', '#D95728')
        .css('border-radius', '25px') //questions[0].question veux dire les questions, selectionne une question

    const section = $('<section></section>') //crée une section dans la page html
    section.append(question) //on a changer $('main') par une constante appelé section
        //^ insère la question
    section.position({ top: 80, left: 50 })
    section.animate({ left: '50px' })

    for (let i = 0; i < questionCourante.reponses.length; i++) {
        const reponse = $('<input class="ml-4 mt-4 row-12"></input>') //boucle sur les réponses
        reponse.attr('type', 'radio') //ajout d'un attribut pour que ce soit un bouton radion !! je dois ajouté un name
        reponse.attr('id', 'radio' + i)
        reponse.attr('name', 'question') //ajoute l'attribut name à question , empèche les 3 boutons d'être coché en même temps
        reponse.attr('value', questionCourante.reponses[i]) //ajoute la valeur
        const libelle = $('<label class="ml-1 row-12 p-2">' + questionCourante.reponses[i] + '</label>') //ajoute libellé au bouton radio
        libelle.attr('for', 'radio' + i) //+i permet de créer de nouveau à chaque fois, suivant le bouclage
        section.append(reponse) //avoir 2 fois 'main' fonctionne, mais n'est pas performant
        section.append(libelle).css('font-size', '1.5rem')
            .css('position', 'absolute')
        section.attr('id', 'section') //!! À FAIRE VÉRIFIER
            //^ insère la réponse
    }
    const bouton = $('<button class="btn ml-4 mt-4 row row-cols">Envoyer la réponse</button>')
    bouton.css('background-color', 'green')
        .css('color', '#071029')
        .css('font-size', '1.5rem')
        .css('border-radius', '25px')
        .css('display', 'block')

    bouton.attr('id', 'soumettre-question')
    bouton.on('click', function() { //quand on clique ça affiche question
        let resultat = []
        let score = 0
            /* function AfficherRésulat() {
            quiz.reponses.is(reponse)
            score++
            resultat.push(score) 
        } */
        quiz.questionCourante += 1
        const reponse = $('input[name=question]:checked', /* '#formulaire' */ ).val()
        quiz.reponses.push(reponse)
        if (quiz.questionCourante >= questions.length) {
            AfficherRapport()
        } else {
            const reponse = $('input[name=question]:checked', /* '#formulaire' */ ).val()
            quiz.reponses.push(reponse)

            $('section').remove() //détruire après avoir pris la réponse

            AfficherQuestionQuiz()
        }
    })
    section.append(bouton)
    $('main').html(section) //.css('background-color', '#D95728') //fait apparaître la section dans le main
}

function AfficherRapport() {
    $('main').html('') //vide le main/la page parce que le quiz et le formulaire est dans la même section dans index
    $('main').append('<p class="mt-5 ml-4 pt-4">', 'Prénom : ' + quiz.prenom + '</p>')
        .append('<p class="mt-5 ml-4">', 'Nom : ' + quiz.nom + '</p>')
        .append('<p class="mt-5 ml-4">', 'Email : ' + quiz.email + '</p>')
        .css('background', 'white')
        .css('border-radius', '50px')
        //afficher numero de questions
        //afficher  question
    $('main').append('<p class="mt-5 ml-4 pb-4">' + quiz.reponses[0] + '</p>') //!! à retravailler, il faut boucler pour faire afficher ce que l'on veux
        .css('font-size', '1.5rem')
    $('main').append('<p>' + resultat + '</p>')
    $('main').append('<td>' + quiz.reponses[0] + '</td>')

}


$(function() {
    $("#dialog-message").css('background-color', '#F1A94A').append('<p>' + 'test' + '</p>')
    $("#dialog-message").dialog({
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });
});

//Affiche la DataTable
$(function() {
    $('#myTable').DataTable();
});


/* function ValiderReponse() {
    if (quiz.reponses.is(reponse)) {
        returns true;
    }
    quantitebonnereponse = +1

}

//afficher les réponses + ajouté un crochet ou x vert à côté de la reponse
*/
$('tbody>td').append('<td>' + quiz.reponses[0] + '</td>')